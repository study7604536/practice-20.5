// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	// Initialize PlayerLife to default value
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PwanCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor) && !SnakeActor->IsTurning)
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirections::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirections::UP;
			SnakeActor->IsTurning = true;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirections::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirections::DOWN;
			SnakeActor->IsTurning = true;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor) && !SnakeActor->IsTurning)
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirections::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirections::RIGHT;
			SnakeActor->IsTurning = true;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirections::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirections::LEFT;
			SnakeActor->IsTurning = true;
		}
	}
}
