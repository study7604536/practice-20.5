// Fill out your copyright notice in the Description page of Project Settings.

#include "Barf.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ABarf::ABarf()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MovementSpeed = 500.f;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ABarf::HandleBeginOverlap);
}

// Called when the game starts or when spawned
void ABarf::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABarf::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FVector NewLocation = GetActorLocation() + GetActorForwardVector() * MovementSpeed * DeltaTime;
	SetActorLocation(NewLocation);
}

void ABarf::Interact(AActor* interactor, bool bIsHead)
{
}

void ABarf::BallElementOverlap(ABarf* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		bool BIsFirst = true;
		Iinteractable* InteractableInterface = Cast<Iinteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, BIsFirst);
		}
	}
}

void ABarf::HandleBeginOverlap(UPrimitiveComponent* OverlapedComponent, AActor* OtherActor,
                               UPrimitiveComponent* OtherComponent, int32 OtherBodyIndedx, bool bFromSweep,
                               const FHitResult& SweepResult)
{
	BallElementOverlap(this, OtherActor);
}
