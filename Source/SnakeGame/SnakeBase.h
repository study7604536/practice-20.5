// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"


class ASnakeElementBase;
class ABarf;

UENUM()
enum class EMovementDirections
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABarf> BallClass;

	UPROPERTY()
	EMovementDirections LastMoveDirection;

	bool IsTurning;

	UFUNCTION(BlueprintCallable, Category = "Snake")
	int GetSnakeLength();

	UFUNCTION(BlueprintCallable, Category = "Snake")
	float GetSnakeLife();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum = 1);
	UFUNCTION(BlueprintCallable, Category = "Snake")
	void RemoveSnakeElement(int ElementsNum = 1);
	void SpawnBall();

	// Move for every custom tick
	void Move();
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	//UPROPERTY for Life variable
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Snake")
	float Life;

	// Function to reset life to maximum value
	void ResetLife();

	// Function to remove life every tick
	void Hunger();

	//how fast the snake's life decreases
	float HungerRate;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
