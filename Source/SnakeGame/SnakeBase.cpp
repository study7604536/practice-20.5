// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "Barf.h"
#include "SnakeElementBase.h"
#include "interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	HungerRate=1.f;
	LastMoveDirection = EMovementDirections::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

int ASnakeBase::GetSnakeLength()
{
	return SnakeElements.Num(); // Return the current length of the snake
}

float ASnakeBase::GetSnakeLife()
{
	return Life;
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move(); // Move the snake every frame
	Hunger(); //Remove snake life every tick
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		if (SnakeElements.Num() > 0)
		{
			NewLocation = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation(); // To spawn element in the back 
		}
		FTransform NewTransform = FTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 Elemintdex = SnakeElements.Add(NewSnakeElem);
		if (Elemintdex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::SpawnBall()
{
	// Spawn a ball based on the head of the snake and its movement direction
	ASnakeElementBase* Head = SnakeElements[0];
	FVector HeadLocation = Head->GetActorLocation();
	FRotator HeadRotation = Head->GetActorRotation();
	FVector SpawnOffset(0.f, 0.f, 0.f);

	switch (LastMoveDirection)
	{
	case EMovementDirections::UP:
		HeadRotation = FRotator(0, 0, 0);
		SpawnOffset.X += 150.f;
		break;
	case EMovementDirections::DOWN:
		HeadRotation = FRotator(0, 180, 0);
		SpawnOffset.X -= 150.f;
		break;
	case EMovementDirections::LEFT:
		HeadRotation = FRotator(0, 90, 0);
		SpawnOffset.Y += 150.f;
		break;
	case EMovementDirections::RIGHT:
		HeadRotation = FRotator(0, -90, 0);
		SpawnOffset.Y -= 150.f;
		break;
	}
	HeadLocation += SpawnOffset;
	FTransform SpawnTransform = FTransform(HeadRotation, HeadLocation);
	ABarf* NewBall = GetWorld()->SpawnActor<ABarf>(BallClass, SpawnTransform);
}

void ASnakeBase::RemoveSnakeElement(int ElementsNum)
{
	if (SnakeElements.Num() - 1 > ElementsNum)
	{
		// Remove specified number of elements from the snake
		for (int i = SnakeElements.Num() - 1; i >= SnakeElements.Num() - ElementsNum; i--)
		{
			SnakeElements[i]->Destroy();
		}
		SnakeElements.SetNum(SnakeElements.Num() - ElementsNum);
	}
	else
	{
		// If there are fewer elements than specified, remove all elements and destroy the snake
		for (int i = SnakeElements.Num() - 1; i >= 0; i--)
		{
			SnakeElements[i]->Destroy();
		}
		this->Destroy();
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	// Determine the movement direction of the snake
	switch (LastMoveDirection)
	{
	case EMovementDirections::UP:
		MovementVector.X += ElementSize;
		IsTurning = false;
		break;
	case EMovementDirections::DOWN:
		MovementVector.X -= ElementSize;
		IsTurning = false;
		break;
	case EMovementDirections::LEFT:
		MovementVector.Y += ElementSize;
		IsTurning = false;
		break;
	case EMovementDirections::RIGHT:
		MovementVector.Y -= ElementSize;
		IsTurning = false;
		break;
	}

	// Rotate the head in the direction of movement
	FRotator NewRotation = MovementVector.Rotation();
	SnakeElements[0]->SetActorRotation(NewRotation);

	//AddActorLocalOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		// Move each element to the position of the previous element
		auto CurrentElement = SnakeElements[i];
		auto PreviousElement = SnakeElements[i - 1];
		FVector PrevLocation = PreviousElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		FRotator PervRotation = PreviousElement->GetActorRotation();
		CurrentElement->SetActorRotation(PervRotation);
	}
	// Move the head of the snake
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	// Check if the overlapped element is valid
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool BIsFirst = ElemIndex == 0;
		Iinteractable* InteractableInterface = Cast<Iinteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, BIsFirst); // Interact with the overlapped element
		}
	}
}


void ASnakeBase::ResetLife()
{
	// Reset life to maximum value
	Life = 1.0f;
}

void ASnakeBase::Hunger()
{
	  // Decrease life over time
    Life -= HungerRate * GetWorld()->GetDeltaSeconds();
	// Check if the snake has died due to hunger
    if (Life <= 0.0f)
    {
        // Handle snake death
        RemoveSnakeElement(GetSnakeLength());
    }
}
