// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "interactable.h"
#include "Barf.generated.h"

class UStaticMeshComponent;
UCLASS()
class SNAKEGAME_API ABarf : public AActor, public Iinteractable
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABarf();

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	virtual void Interact(AActor* interactor, bool bIsHead) override;

	UFUNCTION()
	void BallElementOverlap(ABarf* OverlappedElement, AActor* Other);

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlapedComponent,
	                        AActor* OtherActor,
	                        UPrimitiveComponent* OtherComponent,
	                        int32 OtherBodyIndedx,
	                        bool bFromSweep,
	                        const FHitResult& SweepResult);

private:
	FVector MoveDirection;
};
