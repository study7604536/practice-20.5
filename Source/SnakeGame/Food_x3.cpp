// Fill out your copyright notice in the Description page of Project Settings.


#include "Food_x3.h"
#include "SnakeBase.h"

// Sets default values
AFood_x3::AFood_x3()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AFood_x3::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood_x3::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFood_x3::Interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement(3);
			Snake->ResetLife();
		}
		this->Destroy();
	}
}
