// Fill out your copyright notice in the Description page of Project Settings.


#include "KillerBlock.h"
#include "SnakeBase.h"

// Sets default values
AKillerBlock::AKillerBlock()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AKillerBlock::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AKillerBlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Interaction with snakes head
void AKillerBlock::Interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(interactor);
		if (IsValid((Snake)))
		{
			int SnakeLength = Snake->GetSnakeLength(); 
			Snake->RemoveSnakeElement(SnakeLength);
		}
	}

	if (IsValid(interactor))
	{ 
		interactor->Destroy();
	}
}
