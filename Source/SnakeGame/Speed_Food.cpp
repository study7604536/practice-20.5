// Fill out your copyright notice in the Description page of Project Settings.


#include "Speed_Food.h"
#include "SnakeBase.h"

// Sets default values
ASpeed_Food::ASpeed_Food()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeed_Food::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeed_Food::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeed_Food::Interact(AActor* interactor, bool bIsHead)
{
		if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Snake->MovementSpeed=Snake->MovementSpeed*0.9f;
			Snake->SetActorTickInterval(Snake->MovementSpeed);
			Snake->ResetLife();
			/*int RandX=FMath::RandRange(-900,900);
			int RandY=FMath::RandRange(-1900,1900);
			this->SetActorLocation(FVector(RandX,RandY,0));*/
		}

		this->Destroy();
	}
}

