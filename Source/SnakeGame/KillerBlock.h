// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "interactable.h"
#include "KillerBlock.generated.h"

UCLASS()
class SNAKEGAME_API AKillerBlock : public AActor, public Iinteractable
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AKillerBlock();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Interaction with snakes head
	virtual void Interact(AActor* interactor, bool bIsHead) override;
};
